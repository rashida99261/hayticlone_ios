//
//  topChannelsListVM.swift
//  Eboni
//
//  Created by Apple on 21/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

import UIKit

class topChannelsListVM:BaseTableViewVM{
    
    var vc : followingViewController?
    var items:[Top10_channels] = []
    
    var Filtereditems:[Top10_channels] = []
    
    var arrSelectChannel = [String]()
    
    var viewCntrl : allPublisherListViewController?
    var isTyp = ""
    
    init(controller: UIViewController?, items:[Top10_channels], selectId:[String]) {
        super.init(controller: controller)
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        self.Filtereditems = items
        self.arrSelectChannel = selectId
        self.identifierItem = "TopChannelCell"
        self.nibItem = "TopChannelCell"
        
    }
    
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TopChannelCell", for: indexPath)  as? TopChannelCell else { return UITableViewCell() }
        let obj = self.Filtereditems[indexPath.row]
        cell.setCell(item: obj)
        
        let catId = obj.iD
        if(self.arrSelectChannel.contains(catId!)){
            cell.btnSelect.isSelected = true
        }
        else{
            cell.btnSelect.isSelected = false
        }

        
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.clickOnSelect(_:)), for: .touchUpInside)
        return cell
    }
    
    override func getNumbersOfRows(in section: Int) -> Int {
        return Filtereditems.count
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        
    }
    
    override func willDisplayCell(_ indexPath: IndexPath, tableView: UITableView) {
        
        if(self.isTyp == "all"){
            
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    if indexPath.row == self.Filtereditems.count-1{
                        if(viewCntrl!.fetchingData == false){
                            viewCntrl!.fetchingData = true
                            viewCntrl!.spinner.isHidden = false
                            viewCntrl!.viewFooter.isHidden = false
                            viewCntrl!.hgtFooter.constant = 45
                            self.perform(#selector(loadData), with: nil, afterDelay: 0.5)
                        }
                        /*else{
                         viewContrlr!.viewFooter.isHidden = true
                         viewContrlr!.hgtFooter.constant = 0
                         viewContrlr!.spinner.isHidden = true
                         }*/
                    }
                }
            }
        }
    }
    
    @objc func loadData(){
        viewCntrl!.scrollIndex += 1
        viewCntrl!.callListOfTopTen(strCkeck: "load")
    }

    @objc func clickOnSelect(_ sender: UIButton)
    {
        let obj = self.Filtereditems[sender.tag]
        let catId = obj.iD
        let userId = (userGlobal?.id)!
            
        APIClient.addFollowingItems(userId: userId, cat_id: catId!, objectTyp: "channel") { (response) in
                OperationQueue.main.addOperation {
                    if(response.message == "Your selection has been removed successfully"){
                        for i in 0..<self.arrSelectChannel.count{
                            let val = self.arrSelectChannel[i]
                            if(val == catId){
                                self.arrSelectChannel.remove(at: i)
                                break
                            }
                        }
                        
                        if(self.isTyp == "top"){
                            self.vc?.tblFollowingList.reloadData()
                        }
                        else{
                            self.viewCntrl?.tblChnlFollowingList.reloadData()
                        }
                    }
                    else{
                        self.arrSelectChannel.append(catId!)
                        if(self.isTyp == "top"){
                            self.vc?.tblFollowingList.reloadData()
                        }
                        else{
                            self.viewCntrl?.tblChnlFollowingList.reloadData()
                        }
                    }
                }
            }
        }
}

