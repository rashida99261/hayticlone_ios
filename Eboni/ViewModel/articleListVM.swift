
import UIKit

import GoogleMobileAds

class articleListVM:BaseTableViewVM {
    
    
    var vc : homeViewController?
    
    
    var viewContrlr : publisherListViewController?
    
    var searchVc : searchListViewController?
    
    var typeVC : String = ""
    
    var viewContoller : UIViewController?
    
    var cat_id: String?
    
    var items:[AnyObject] = []
    
    var isDataLoad = false
    
    var heightConstraint: NSLayoutConstraint?
    
    init(controller: UIViewController?, items:[AnyObject]) {
        super.init(controller: controller)
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        self.identifierItem = "articleListCell"
        self.nibItem = "articleListCell"
        
    }
    
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
        if let obj = self.items[indexPath.row] as? NSDictionary{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "articleListCell", for: indexPath)  as? articleListCell else { return UITableViewCell() }
            
            
            cell.setCell(item: obj)
            
            cell.bannerHgt.constant = 0
            cell.stackBanner.isHidden = true
            
            if let colorCode = obj["color_code"] as? String{
                let replaced = colorCode.replacingOccurrences(of: "#", with: "0x")
                let result = UInt32(String((replaced.dropFirst(2))), radix: 16)
                cell.lblCatName.textColor = vc!.UIColorFromHex(rgbValue: result!, alpha: 1.0)
            }
            else{
                if let mode = userDef.value(forKey: "isMode") as? String
                {
                    if(mode == "light"){
                        cell.lblCatName.textColor = .black
                    }
                    else if(mode == "dark"){
                        cell.lblCatName.textColor = .white
                    }
                    else if(mode == "auto"){
                        
                        if #available(iOS 13.0, *) {
                            if UITraitCollection.current.userInterfaceStyle == .dark {
                                cell.lblCatName.textColor = .black
                            }
                            else {
                                cell.lblCatName.textColor = .white
                            }
                        }
                        
                    }
                }
                else{
                    if #available(iOS 13.0, *) {
                        if UITraitCollection.current.userInterfaceStyle == .dark {
                            cell.lblCatName.textColor = .black
                        }
                        else {
                            cell.lblCatName.textColor = .white
                        }
                    }
                }
            }
            
            if(typeVC == "home"){
                cell.btnSeeAll.isHidden = false
                cell.btnSeeAll.tag = indexPath.row
                cell.btnSeeAll.addTarget(self, action: #selector(self.clickOnSeeBtn(_:)), for: .touchUpInside)
                
            }
            else{
                cell.btnSeeAll.isHidden = true
            }
            
            cell.btnPublsih.tag = indexPath.row
            cell.btnPublsih.addTarget(self, action: #selector(self.imageTapped(_:)), for: .touchUpInside)
            
            cell.btnFolder.tag = indexPath.row
            cell.btnFolder.addTarget(self, action: #selector(self.clickOnFolderBtn(_:)), for: .touchUpInside)
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(self.clickOnShare(_:)), for: .touchUpInside)
            
            
            let likeC = obj["total_likes"] as? String
            cell.lblLikeCount.text = "\(likeC ?? "0")"
            
            
            let fav = obj["isFavorite"] as? String ?? "0"
            if(fav == "0"){
                cell.imgLikes.image = UIImage(named: "unlike")
            }else{
                cell.imgLikes.image = UIImage(named: "like")
            }
            
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.clickOnTopLike(_:)), for: .touchUpInside)
            
            return cell
            
        }
        else{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "bannerAdsCell", for: indexPath)  as? bannerAdsCell else { return UITableViewCell() }
            
            let banner = cell.loadBanner(rootvc: self.viewContoller!, frame: cell.contntView.bounds)
            
            
            for view in cell.contntView.subviews{
                if view.isKind(of: GADBannerView.self){
                    view.removeFromSuperview()
                }
            }
            
            cell.contntView.addSubview(banner)
            
            banner.center = cell.contntView.center
            
           let request = GADRequest()
            banner.load(request)
            
          return cell
    }

//        else {
//            let nativeAd = self.items[indexPath.row] as! GADUnifiedNativeAd
//            /// Set the native ad's rootViewController to the current view controller.
//            nativeAd.rootViewController = vc
//
//            // guard let cell = tableView.dequeueReusableCell(withIdentifier: "unifiedNativeAdCell", for: indexPath) as? unifiedNativeAdCell else { return UITableViewCell() }
//            let nativeAdCell = tableView.dequeueReusableCell(
//                withIdentifier: "UnifiedNativeAdCell", for: indexPath)
//
//            // Get the ad view from the Cell. The view hierarchy for this cell is defined in
//            // UnifiedNativeAdCell.xib.
//            let adView : GADUnifiedNativeAdView = nativeAdCell.contentView.subviews
//                .first as! GADUnifiedNativeAdView
//
//            // Associate the ad view with the ad object.
//            // This is required to make the ad clickable.
//            adView.nativeAd = nativeAd
//
//            // Populate the ad view with the ad assets.
//            (adView.headlineView as! UILabel).text = nativeAd.headline
//            (adView.priceView as! UILabel).text = nativeAd.price
//
//            if let starRating = nativeAd.starRating {
//                (adView.starRatingView as! UILabel).text =
//                    starRating.description + "\u{2605}"
//            } else {
//                (adView.starRatingView as! UILabel).text = nil
//            }
//
//            (adView.iconView as? UIImageView)?.image = nativeAd.icon?.image
//            adView.iconView?.isHidden = nativeAd.icon == nil
//
//            if let mediaView = adView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
//                heightConstraint = NSLayoutConstraint(
//                    item: mediaView,
//                    attribute: .height,
//                    relatedBy: .equal,
//                    toItem: mediaView,
//                    attribute: .width,
//                    multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
//                    constant: 0)
//                heightConstraint?.isActive = true
//            }
//
//
//            (adView.bodyView as! UILabel).text = nativeAd.body
//            (adView.advertiserView as! UILabel).text = nativeAd.advertiser
//            // The SDK automatically turns off user interaction for assets that are part of the ad, but
//            // it is still good to be explicit.
//            (adView.callToActionView as! UIButton).isUserInteractionEnabled = false
//            (adView.callToActionView as! UIButton).setTitle(
//                nativeAd.callToAction, for: .normal)
//
//            return nativeAdCell
//        }
        
        //return tbCell
    }
    
    override func getNumbersOfRows(in section: Int) -> Int {
        return items.count
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        if let _ = self.items[indexPath.row] as? NSDictionary {
            return UITableView.automaticDimension
        }
        else{
            return 260
        }
    }
    
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        let obj = self.items[indexPath.row] as! NSDictionary
        self.countClickView(obj: obj)
    }
    
    override func willDisplayCell(_ indexPath: IndexPath, tableView: UITableView) {
        
        if(typeVC == "home_cat"){
            
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    if indexPath.row == items.count - 1 {
                        // let totalHt = cell.frame.height * 9
                        if(vc!.isFetchingCat == false){
                            vc!.isFetchingCat = true
                            vc!.spinner.isHidden = false
                            vc!.viewCatFooter.isHidden = false
                            vc!.hgtCatFooter.constant = 45
                            self.perform(#selector(loadData), with: nil, afterDelay: 2)
                        }
                        /*else{
                         vc!.viewCatFooter.isHidden = true
                         vc!.hgtCatFooter.constant = 0
                         vc!.spinner.isHidden = true
                         }*/
                    }
                }
            }
        }
        else if(typeVC == "publish"){
            
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    if indexPath.row == self.items.count-1{
                        if(viewContrlr!.isFetching == false){
                            viewContrlr!.isFetching = true
                            viewContrlr!.spinner.isHidden = false
                            viewContrlr!.viewFooter.isHidden = false
                            viewContrlr!.hgtFooter.constant = 45
                            self.perform(#selector(loadData), with: nil, afterDelay: 2)
                        }
                        /*else{
                         viewContrlr!.viewFooter.isHidden = true
                         viewContrlr!.hgtFooter.constant = 0
                         viewContrlr!.spinner.isHidden = true
                         }*/
                    }
                }
            }
        }
        else if(typeVC == "search"){
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    // do here...
                    if indexPath.row == self.items.count-1{
                        if(searchVc!.fetchingSearchData == false){
                            searchVc!.fetchingSearchData = true
                            searchVc!.spinner.isHidden = false
                            searchVc!.viewFooter.isHidden = false
                            searchVc!.hgtFooter.constant = 45
                            self.perform(#selector(loadData), with: nil, afterDelay: 2)
                        }
                    }
                }
            }
        }
    }
    
    @objc func loadData(){
        
        if(typeVC == "publish"){
            viewContrlr!.scrollIndex += 1
            viewContrlr!.setPublishArticlesApi(strCkeck: "load")
        }
        else if(typeVC == "home_cat"){
            vc!.scrollIndex_cat += 1
            vc!.setbelowArticlesApiWithCatId(strCkeck: "load", cat_id: cat_id!)
        }
        else if(typeVC == "search"){
            searchVc?.scrollIndex += 1
            searchVc?.seearchArticlesApi(strCkeck: "load", strText: (searchVc?.searchText.text!)!)
        }
    }
    
    @objc func imageTapped(_ sender: UIButton) {
        
        if(typeVC == "home" || typeVC == "home_cat" || typeVC == "search"){
            let obj = self.items[sender.tag] as! NSDictionary
            
            let publisher_id = obj["publisher_id"] as? String
            let nid = obj["nid"] as? String
            let isFavorite = obj["isFavorite"] as? String
            let total_likes = obj["total_likes"] as? String
            let nodealias = obj["nodealias"] as? String
            
            self.passToViewController(strUrl: publisher_id!, strType: "publisher", strArticleId: nid!, strFav: isFavorite!, strLikeC: total_likes ?? "0", strAlias: nodealias ?? "/")
        }
        else if(typeVC == "publish"){
        }
    }
    
    
    
    @objc func clickOnFolderBtn(_ sender: UIButton)
    {
        if(typeVC == "home" || typeVC == "home_cat"){
            if(userGlobal?.id == nil){
                let detail = vc!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = vc!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                vc!.present(navC, animated: true, completion: nil)
            }
            else{
                
                let detail = vc!.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
                detail.modalPresentationStyle = .overCurrentContext
                let obj = self.items[sender.tag] as! NSDictionary
                let nid = obj["nid"] as? String
                detail.articleId = nid!
                detail.openFrom = "add"
                vc!.present(detail, animated: true, completion: nil)
            }
        }
        else if(typeVC == "publish"){
            if(userGlobal?.id == nil){
                let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = viewContrlr!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                viewContrlr!.present(navC, animated: true, completion: nil)

            }
            else{
                let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
                detail.modalPresentationStyle = .overCurrentContext
                let obj = self.items[sender.tag] as! NSDictionary
                let nid = obj["nid"] as? String
                detail.articleId = nid!
                detail.openFrom = "add"
                viewContrlr!.present(detail, animated: true, completion: nil)
            }
            
        }
        else if(typeVC == "search"){
            if(userGlobal?.id == nil){
                let detail = searchVc!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = searchVc!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                searchVc!.present(navC, animated: true, completion: nil)

            }
            else{
                let detail = searchVc!.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
                detail.modalPresentationStyle = .overCurrentContext
                let obj = self.items[sender.tag] as! NSDictionary
                let nid = obj["nid"] as? String
                detail.articleId = nid!
                detail.openFrom = "add"
                searchVc!.present(detail, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func clickOnShare(_ sender: UIButton)
    {
        if(typeVC == "home" || typeVC == "home_cat"){
            let obj = self.items[sender.tag] as! NSDictionary
            
            print(obj)
            
            let nid = obj["nid"] as? String
            let full_url = obj["full_url"] as? String
            let title = obj["title"] as? String
            let nodealias = obj["nodealias"] as? String
            
            let detail = vc!.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
            detail.modalPresentationStyle = .overCurrentContext
            detail.strLink = full_url!
            detail.nid = nid!
            detail.strTitle = title!
            detail.nodeAlias = nodealias!
            vc!.present(detail, animated: true, completion: nil)
        }
        else if(typeVC == "publish"){
            let obj = self.items[sender.tag] as! NSDictionary
            let nid = obj["nid"] as? String
            let full_url = obj["full_url"] as? String
            let title = obj["title"] as? String
            let nodealias = obj["nodealias"] as? String
           
            let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
            detail.modalPresentationStyle = .overCurrentContext
            detail.strLink = full_url!
            detail.nid = nid!
            detail.strTitle = title!
            detail.nodeAlias = nodealias!
            viewContrlr!.present(detail, animated: true, completion: nil)
            
        }
        else if(typeVC == "search"){
            let obj = self.items[sender.tag] as! NSDictionary
            let nid = obj["nid"] as? String
            let full_url = obj["full_url"] as? String
            let title = obj["title"] as? String
            let nodealias = obj["nodealias"] as? String
            
            
            let detail = searchVc!.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
            detail.modalPresentationStyle = .overCurrentContext
            detail.strLink = full_url!
            detail.nid = nid!
            detail.strTitle = title!
            detail.nodeAlias = nodealias!
            searchVc!.present(detail, animated: true, completion: nil)
        }
    }
    
    
    @objc func clickOnTopLike(_ sender: UIButton){
        if(typeVC == "home"){
            if(userGlobal?.id == nil){
                let detail = vc!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = vc!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                vc!.present(navC, animated: true, completion: nil)

            }
            else{
                let obj = self.items[sender.tag] as! NSDictionary
                
                let newObj = obj.mutableCopy() as! NSMutableDictionary
                
                let nid = obj["nid"] as? String
                var isFavorite = obj["isFavorite"] as? String
                var total_likes = obj["total_likes"] as? String
                
                let buttonPosition = sender.convert(CGPoint.zero, to: vc?.tblBelowArticleList)
                let indexPath = vc?.tblBelowArticleList.indexPathForRow(at: buttonPosition)
                let cell = vc?.tblBelowArticleList.cellForRow(at: indexPath!) as! articleListCell
                
                let fav = isFavorite ?? "0"
                if(fav == "0"){
                    isFavorite = "1"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    count! += 1
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                    
                }else{
                    isFavorite = "0"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    
                    if(count! > 0){
                        count! -= 1
                    }else{
                        count = 0
                    }
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                }
                
                self.items.remove(at: sender.tag)
                self.items.insert(newObj, at: sender.tag)
                
                vc?.tblBelowArticleList.reloadData()
                
                var userId = ""
                if(userGlobal?.id == nil){
                    userId = "0"
                }
                else{
                    userId = (userGlobal?.id)!
                }
                
                APIClient.likeonArticle(userId: userId, node_id: nid!, likes: "1") { (resp) in
                    DispatchQueue.main.async { [self] in
                        vc?.tblBelowArticleList.reloadData()
                    }
                }
            }
        }
        else if typeVC == "home_cat" {
            
            if(userGlobal?.id == nil){
                let detail = vc!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = vc!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                vc!.present(navC, animated: true, completion: nil)

            }
            else{
                
                let obj = self.items[sender.tag] as! NSDictionary
                let newObj = obj.mutableCopy() as! NSMutableDictionary
                let nid = obj["nid"] as? String
                var isFavorite = obj["isFavorite"] as? String
                var total_likes = obj["total_likes"] as? String
                
                let buttonPosition = sender.convert(CGPoint.zero, to: vc?.tblBelowArticleListCat)
                let indexPath = vc?.tblBelowArticleListCat.indexPathForRow(at: buttonPosition)
                let cell = vc?.tblBelowArticleListCat.cellForRow(at: indexPath!) as! articleListCell
                
                let fav = isFavorite ?? "0"
                if(fav == "0"){
                    isFavorite = "1"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    count! += 1
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                    
                }else{
                    isFavorite = "0"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    
                    if(count! > 0){
                        count! -= 1
                    }else{
                        count = 0
                    }
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                }
                
                self.items.remove(at: sender.tag)
                self.items.insert(newObj, at: sender.tag)
                
                vc?.tblBelowArticleListCat.reloadData()
                
                var userId = ""
                if(userGlobal?.id == nil){
                    userId = "0"
                }
                else{
                    userId = (userGlobal?.id)!
                }
                
                APIClient.likeonArticle(userId: userId, node_id: nid!, likes: "1") { (resp) in
                    DispatchQueue.main.async { [self] in
                        vc?.tblBelowArticleListCat.reloadData()
                    }
                }
            }
        }
        else if(typeVC == "publish"){
            
            if(userGlobal?.id == nil){
                let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = viewContrlr!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                viewContrlr!.present(navC, animated: true, completion: nil)

            }
            else{
                let obj = self.items[sender.tag] as! NSDictionary
                let newObj = obj.mutableCopy() as! NSMutableDictionary
                let nid = obj["nid"] as? String
                var isFavorite = obj["isFavorite"] as? String
                var total_likes = obj["total_likes"] as? String
                
                let buttonPosition = sender.convert(CGPoint.zero, to: viewContrlr?.tblPublishArticleList)
                let indexPath = viewContrlr?.tblPublishArticleList.indexPathForRow(at: buttonPosition)
                let cell = viewContrlr?.tblPublishArticleList.cellForRow(at: indexPath!) as! articleListCell
                
                let fav = isFavorite ?? "0"
                if(fav == "0"){
                    isFavorite = "1"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    count! += 1
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                    
                }else{
                    isFavorite = "0"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    
                    if(count! > 0){
                        count! -= 1
                    }else{
                        count = 0
                    }
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    newObj.setValue(isFavorite, forKey: "isFavorite")
                    newObj.setValue(total_likes, forKey: "total_likes")
                    
                }
                
                self.items.remove(at: sender.tag)
                self.items.insert(newObj, at: sender.tag)
                
                viewContrlr?.tblPublishArticleList.reloadData()
                
                var userId = ""
                if(userGlobal?.id == nil){
                    userId = "0"
                }
                else{
                    userId = (userGlobal?.id)!
                }
                
                APIClient.likeonArticle(userId: userId, node_id: nid!, likes: "1") { (resp) in
                    DispatchQueue.main.async { [self] in
                        viewContrlr?.tblPublishArticleList.reloadData()
                    }
                }
            }
        }
        else if(typeVC == "search"){
            
            if(userGlobal?.id == nil){
                let detail = searchVc!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = searchVc!.self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                viewContrlr!.present(navC, animated: true, completion: nil)

            }
            else{
                
                let obj = self.items[sender.tag] as! NSDictionary
                
                var isFavorite = obj["isFavorite"] as? String
                var total_likes = obj["total_likes"] as? String
                
                
                let buttonPosition = sender.convert(CGPoint.zero, to: searchVc?.tblArticleList)
                let indexPath = searchVc?.tblArticleList.indexPathForRow(at: buttonPosition)
                let cell = searchVc?.tblArticleList.cellForRow(at: indexPath!) as! articleListCell
                
                let fav = isFavorite ?? "0"
                if(fav == "0"){
                    isFavorite = "1"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    count! += 1
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                    
                    
                }else{
                    isFavorite = "0"
                    
                    let likeC = total_likes
                    var count = Int(likeC ?? "0")
                    
                    if(count! > 0){
                        count! -= 1
                    }else{
                        count = 0
                    }
                    cell.lblLikeCount.text = "\(count ?? 0)"
                    total_likes = "\(count ?? 0)"
                }
                
                self.items.remove(at: sender.tag)
                self.items.insert(obj, at: sender.tag)
                
                searchVc?.tblArticleList.reloadData()
                
                var userId = ""
                if(userGlobal?.id == nil){
                    userId = "0"
                }
                else{
                    userId = (userGlobal?.id)!
                }
                
                let nid = obj["nid"] as? String
                
                APIClient.likeonArticle(userId: userId, node_id: nid!, likes: "1") { (resp) in
                    DispatchQueue.main.async { [self] in
                        searchVc?.tblArticleList.reloadData()
                    }
                }
            }
        }
    }
    
    
    func passToViewController(strUrl: String, strType: String, strArticleId: String, strFav: String, strLikeC: String, strAlias: String)
    {
        // isBackFrom = "other"
        if(strType == "cell"){
            if(typeVC == "home" || typeVC == "home_cat"){
                let detail = vc!.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
                detail.passUrl = strUrl
                detail.passArticleId = strArticleId
                detail.isFav = strFav
                detail.likeCount = strLikeC
                detail.nodealias = strAlias
                vc!.navigationController?.pushViewController(detail, animated: true)
            }
            else if(typeVC == "publish"){
                let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
                detail.passUrl = strUrl
                detail.passArticleId = strArticleId
                detail.isFav = strFav
                detail.likeCount = strLikeC
                detail.nodealias = strAlias
                viewContrlr!.navigationController?.pushViewController(detail, animated: true)
            }
            else if(typeVC == "search"){
                let detail = searchVc!.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
                detail.passUrl = strUrl
                detail.passArticleId = strArticleId
                detail.isFav = strFav
                detail.likeCount = strLikeC
                detail.nodealias = strAlias
                searchVc!.navigationController?.pushViewController(detail, animated: true)
            }
        }
        else if(strType == "publisher"){
            if(typeVC == "search"){
                let detail = searchVc!.storyboard?.instantiateViewController(identifier: "publisherListViewController") as! publisherListViewController
                detail.publishrId = strUrl
                searchVc!.navigationController?.pushViewController(detail, animated: true)
            }
            else if(typeVC == "home" || typeVC == "home_cat"){
                let detail = vc!.storyboard?.instantiateViewController(identifier: "publisherListViewController") as! publisherListViewController
                detail.publishrId = strUrl
                vc!.navigationController?.pushViewController(detail, animated: true)
            }
        }
    }
    
    @objc func clickOnSeeBtn(_ sender: UIButton) {
        let obj = self.items[sender.tag] as! NSDictionary
        
        let category_id = obj["category_id"] as? String
        let category_name = obj["category_name"] as? String
        
        if(typeVC == "home"){
            vc?.callApiWhenClickonSeeAllBtn(category_id: category_id!, cat_name: category_name!)
        }
    }
    
    func countClickView(obj : NSDictionary){
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        let nid = obj["nid"] as? String
        let full_url = obj["full_url"] as? String
        let isFavorite = obj["isFavorite"] as? String
        let total_likes = obj["total_likes"] as? String
        let nodealias = obj["nodealias"] as? String
        
        APIClient.countClickViewsonArticle(userId: userId, node_id: nid!) { (response) in
            OperationQueue.main.addOperation{
                self.passToViewController(strUrl: full_url!, strType: "cell", strArticleId: nid!, strFav: isFavorite!, strLikeC: total_likes ?? "0", strAlias: nodealias ?? "/")
            }
        }
    }
}
