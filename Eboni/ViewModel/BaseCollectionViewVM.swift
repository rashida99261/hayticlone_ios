//
//  BaseCollectionViewVM.swift
//  U-Experience
//
//  Created by gianni murillo anziani on 3/4/19.
//  Copyright © 2019 codefit. All rights reserved.
//

import Foundation
import UIKit


import Foundation
import UIKit
class BaseCollectionViewVM:NSObject{
    
    var headerHeight:CGFloat = 0
    var identifierItem = ""
    var nibItem = ""
    var actualController:UIViewController!
    var actualCell:UICollectionViewCell!
    var collectionItems:[Any] = [Any]()
    var sections:[String] = []
    var baseHeaderCollectionViewHeight:CGFloat = 0
    init(controller:UIViewController?) {
        self.actualController = controller
    }
    init(cell:UICollectionViewCell?) {
        self.actualCell = cell
    }
}

//TABLE_VIEW_METHODS
extension BaseCollectionViewVM{

    @objc func getReferenceSizeForHeaderInSection(_ collectionView: UICollectionView, collectionViewLayout: UICollectionViewLayout, section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: headerHeight)
    }
    
    @objc func btn_headerPressed(sender:UIButton){
        
    }
    @objc func getHeightForRowAt(_ indexPath:IndexPath)->CGFloat{
        return 0
    }
    
    @objc func getItemForRowAt(_ indexPath:IndexPath, collectionView:UICollectionView)->UICollectionViewCell{
        
        return UICollectionViewCell()
    }
    @objc func getNumbersOfItemsForSection(_ section:Int)->Int{
        return collectionItems.count
    }
    @objc func getNumbersOfSections()->Int{
        return sections.count
    }
    @objc func getSelectRowAt(_ indexPath: IndexPath, collectionView: UICollectionView) {
    }
}
