
import UIKit
import GoogleMobileAds

class articleByFolderListVM:BaseTableViewVM{
    
    
    var viewContrlr : articleByFolderVC?
    
    var cat_id: String?
    
    var items:[AnyObject] = []
    
    var heightConstraint: NSLayoutConstraint?
    
    init(controller: UIViewController?, items:[AnyObject]) {
        super.init(controller: controller)
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        self.identifierItem = "FolderArticleCell"
        self.nibItem = "FolderArticleCell"
        
    }
    
    
    override func getCellForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        
         let obj = self.items[indexPath.row] as! NSDictionary
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FolderArticleCell", for: indexPath)  as? FolderArticleCell else { return UITableViewCell() }
            
            cell.setCell(item: obj)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
            cell.imgPublisher.tag = indexPath.row
            cell.imgPublisher.isUserInteractionEnabled = true
            cell.imgPublisher.addGestureRecognizer(tapGestureRecognizer)
            
            cell.btnFolder.tag = indexPath.row
            cell.btnFolder.addTarget(self, action: #selector(self.clickOnFolder(_:)), for: .touchUpInside)
            
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(clickOnSetingBtn(_:)), for: .touchUpInside)
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(clickOnFolderShare(_:)), for: .touchUpInside)
            
            cell.btnPublsih.tag = indexPath.row
            cell.btnPublsih.addTarget(self, action: #selector(self.imageTapped(_:)), for: .touchUpInside)
            
            
            let likeC = obj["total_likes"] as? String
            cell.lblLikeCount.text = "\(likeC ?? "0")"
            
            
            let fav = obj["isFavorite"] as? String ?? "0"
            if(fav == "0"){
                cell.imgLikes.image = UIImage(named: "unlike")
            }else{
                cell.imgLikes.image = UIImage(named: "like")
            }
            
            
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.clickOnTopLike(_:)), for: .touchUpInside)
            
            
            
            return cell

        
        
    }
    
    override func getNumbersOfRows(in section: Int) -> Int {
        return items.count
    }
    
    override func getHeightForRowAt(_ indexPath: IndexPath, tableView: UITableView) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func didSelectRowAt(_ indexPath: IndexPath, tableView: UITableView) {
        
        let obj = self.items[indexPath.row]
        
        let full_url = obj["full_url"] as? String
        let article_id = obj["article_id"] as? String
        let isFavorite = obj["isFavorite"] as? String
        let total_likes = obj["total_likes"] as? String
        let nodealias = obj["nodealias"] as? String

        self.passToViewController(strUrl: full_url!, strType: "cell", strArticleId: article_id!, strFav: isFavorite!, strLikeC: total_likes!, strAlias: nodealias ?? "/")
    }
    
    override func willDisplayCell(_ indexPath: IndexPath, tableView: UITableView) {
        
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                // do here...
                if indexPath.row == self.items.count-1{
                    if(viewContrlr!.isFetching == false){
                        viewContrlr!.isFetching = true
                        viewContrlr!.spinner.isHidden = false
                        viewContrlr!.viewFooter.isHidden = false
                        viewContrlr!.hgtFooter.constant = 45
                        self.perform(#selector(loadData), with: nil, afterDelay: 2)
                    }
                    /*else{
                     viewContrlr!.viewFooter.isHidden = true
                     viewContrlr!.hgtFooter.constant = 0
                     viewContrlr!.spinner.isHidden = true
                     }*/
                }
            }
        }
        
       
        
        
    }
    
    @objc func clickOnSetingBtn(_ sender: UIButton)
    {
        viewContrlr?.openFolderPopUp(tag: sender.tag)
    }
    
    @objc func loadData(){
        viewContrlr!.scrollIndex += 1
        viewContrlr!.setFolderArticlesApi(strCkeck: "load")
    }
    
    @objc func imageTapped(_ sender: UIButton) {
        
        let obj = self.items[sender.tag] as! NSDictionary
        
        let publisher_id = obj["publisher_id"] as? String
        let article_id = obj["article_id"] as? String
        let isFavorite = obj["isFavorite"] as? String
        let total_likes = obj["total_likes"] as? String
        let nodealias = obj["nodealias"] as? String

        self.passToViewController(strUrl: publisher_id!, strType: "publisher", strArticleId: article_id!, strFav: isFavorite!, strLikeC: total_likes!, strAlias: nodealias ?? "/")
    }
    
    @objc func clickOnFolder(_ sender: UIButton)
    {
        if(userGlobal?.id == nil){
            let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
            detail.modalPresentationStyle = .overCurrentContext
            let navC = UINavigationController(rootViewController: detail)
            navC.navigationBar.isHidden = true
            viewContrlr!.present(navC, animated: true, completion: nil)

        }
        else{
            let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
            detail.modalPresentationStyle = .overCurrentContext
            let obj = self.items[sender.tag] as! NSDictionary
            let article_id = obj["article_id"] as? String
            detail.articleId = article_id!
            detail.openFrom = "add"
            viewContrlr!.present(detail, animated: true, completion: nil)
        }
    }
    
    
    @objc func clickOnFolderShare(_ sender: UIButton)
    {
        let obj = self.items[sender.tag] as! NSDictionary
        
        let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
        detail.modalPresentationStyle = .overCurrentContext
        
        let full_url = obj["full_url"] as? String
        let article_id = obj["article_id"] as? String
        let nodealias = obj["nodealias"] as? String
        
        detail.strLink = full_url!
        detail.nid = article_id!
        detail.nodeAlias = nodealias!
        viewContrlr!.present(detail, animated: true, completion: nil)
    }
    
    func passToViewController(strUrl: String, strType: String, strArticleId: String,strFav: String, strLikeC: String, strAlias: String)
    {
        if(strType == "cell"){
            let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
            detail.passUrl = strUrl
            detail.passArticleId = strArticleId
            detail.isFav = strFav
            detail.likeCount = strLikeC
            detail.nodealias = strAlias
            viewContrlr!.navigationController?.pushViewController(detail, animated: true)
        }
        else if(strType == "publisher"){
            let detail = viewContrlr!.storyboard?.instantiateViewController(identifier: "publisherListViewController") as! publisherListViewController
            detail.publishrId = strUrl
            viewContrlr!.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    @objc func clickOnTopLike(_ sender: UIButton){
        
        let obj = self.items[sender.tag] as! NSDictionary
        
        let newObj = obj.mutableCopy() as! NSMutableDictionary
        
        let article_id = obj["article_id"] as? String
        var isFavorite = obj["isFavorite"] as? String
        var total_likes = obj["total_likes"] as? String

        
        let buttonPosition = sender.convert(CGPoint.zero, to: viewContrlr?.tblFolderArticleList)
        let indexPath = viewContrlr?.tblFolderArticleList.indexPathForRow(at: buttonPosition)
        let cell = viewContrlr?.tblFolderArticleList.cellForRow(at: indexPath!) as! FolderArticleCell
        

        let fav = isFavorite ?? "0"
        if(fav == "0"){
            isFavorite = "1"
            
            let likeC = total_likes
            var count = Int(likeC ?? "0")
            count! += 1
            cell.lblLikeCount.text = "\(count ?? 0)"
            total_likes = "\(count ?? 0)"
            
            newObj.setValue(isFavorite, forKey: "isFavorite")
            newObj.setValue(total_likes, forKey: "total_likes")
            
            
        }else{
            isFavorite = "0"
            
            let likeC = total_likes
            var count = Int(likeC ?? "0")
            
            if(count! > 0){
                count! -= 1
            }else{
                count = 0
            }
            cell.lblLikeCount.text = "\(count ?? 0)"
            total_likes = "\(count ?? 0)"
            
            newObj.setValue(isFavorite, forKey: "isFavorite")
            newObj.setValue(total_likes, forKey: "total_likes")
            
        }
        
        self.items.remove(at: sender.tag)
        self.items.insert(newObj, at: sender.tag)

        viewContrlr?.tblFolderArticleList.reloadData()
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        APIClient.likeonArticle(userId: userId, node_id: article_id!, likes: "1") { (resp) in
            DispatchQueue.main.async { [self] in
                viewContrlr?.tblFolderArticleList.reloadData()
            }
        }
    }
}
