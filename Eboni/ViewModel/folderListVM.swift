import UIKit

class folderListVM: BaseCollectionViewVM {
    
    var vc : foldersListViewController?
    var items:[allFolderList] = []
    
    init(controller: UIViewController?, items:[allFolderList]) {
        super.init(controller: controller)
        headerHeight = 0
        if self.items.count>0{
            self.items.removeAll()
        }
        self.items = items
        collectionItems = self.items
        
        nibItem = "folderListCell"
        identifierItem = "folderListCell"

    }

    override func getItemForRowAt(_ indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierItem, for: indexPath) as! folderListCell
        
        let dict = self.items[indexPath.row]
        
        cell.btnSetting.tag = indexPath.row
        cell.btnSetting.addTarget(self, action: #selector(clickOnSetingBtn(_:)), for: .touchUpInside)
        
        cell.btnBlur.tag = indexPath.row
        cell.btnBlur.addTarget(self, action: #selector(clickOnCellBtn(_:)), for: .touchUpInside)

        
        cell.setCell(item: dict)
        return cell
    }
    
    override func getSelectRowAt(_ indexPath: IndexPath, collectionView: UICollectionView)
    {
    }
    
    func getSizeForItem(_ collectionView: UICollectionView, collectionViewLayout: UICollectionViewLayout, indexPath: IndexPath) -> CGSize
    {
        var itemHeight : CGFloat = 0.0
        if UIDevice.current.userInterfaceIdiom == .phone {
            itemHeight = 150
        }
        else if UIDevice.current.userInterfaceIdiom == .pad {
            itemHeight = 350
        }
        return CGSize(width: collectionView.frame.size.width/2, height: itemHeight)
    }
    
    @objc func clickOnSetingBtn(_ sender: UIButton)
    {
        vc?.openFolderPopUp(tag: sender.tag)
    }
    
    @objc func clickOnCellBtn(_ sender: UIButton)
    {
        let dict = self.items[sender.tag]
        let detail = vc!.storyboard?.instantiateViewController(identifier: "articleByFolderVC") as! articleByFolderVC
        detail.folderId = dict.id!
        detail.folderName = dict.folder_name!
        vc!.navigationController?.pushViewController(detail, animated: true)
    }

}
