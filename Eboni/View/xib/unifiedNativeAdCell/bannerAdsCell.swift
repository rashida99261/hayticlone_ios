//
//  bannerAdsCell.swift
//  Eboni
//
//  Created by Apple on 04/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import GoogleMobileAds

class bannerAdsCell: UITableViewCell {
    
    @IBOutlet weak var contntView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadBanner(rootvc : UIViewController , frame: CGRect) -> GADBannerView{
        
        let bannerView = GADBannerView()
        bannerView.rootViewController = rootvc
        bannerView.adUnitID = Endpoints.Environment.googleAdsBannerCell
        bannerView.adSize = kGADAdSizeMediumRectangle
        return bannerView

    }

}
