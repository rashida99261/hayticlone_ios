

import UIKit


//class : UIView {
//
//    let nibName = "CustomfollowingHeaderview"
//    var contentView: UIView?
//
//    @IBOutlet weak var lblTitle : UILabel!
//    @IBOutlet weak var btnShowAll : UIButton!
//
//    public required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        guard let view = loadViewFromNib() else { return }
//        view.frame = self.bounds
//        self.addSubview(view)
//
//        view.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            view.topAnchor.constraint(equalTo: self.topAnchor),
//            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
//            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
//            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
//        ])
//
//        contentView = view
//        view.frame = self.bounds
//
//    }
//    func loadViewFromNib() -> UIView? {
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: nibName, bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil).first as? UIView
//    }
//}

 class CustomfollowingHeaderview: UITableViewHeaderFooterView {
    
    static let reuseIdentifier = "CustomfollowingHeaderview"
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnShowAll : UIButton!
    @IBOutlet weak var lblsubtitle : UILabel!


    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
