
import UIKit

class TopTopicsCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    
    func setCell(item:Top10_topics){
        self.lblName.text = item.cat_name
    }

}
