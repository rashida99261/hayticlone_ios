
import Foundation
import UIKit
struct Colors {
    
    //Colors
    
    //THEME

    static var WHITE = UIColor.colorFromHex(rgbValue: 0xFFFFFF)
    static var BLACK = UIColor.colorFromHex(rgbValue: 0x000000)
 
    
    
    static var THEME_COLOR = UIColor.colorFromHex(rgbValue: 0xED1C24)
    static var GRAY_COLOR = UIColor.colorFromHex(rgbValue: 0x7A8393)
    
}

