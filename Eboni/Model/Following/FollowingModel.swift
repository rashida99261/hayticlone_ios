

import Foundation

struct FollowingModel : Codable {
	let top10_topics : [Top10_topics]?
	let top10_channels : [Top10_channels]?
	let selected_topics : [String]?
	let selected_channels : [String]?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case top10_topics = "top10_topics"
		case top10_channels = "top10_channels"
		case selected_topics = "selected_topics"
		case selected_channels = "selected_channels"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		top10_topics = try values.decodeIfPresent([Top10_topics].self, forKey: .top10_topics)
		top10_channels = try values.decodeIfPresent([Top10_channels].self, forKey: .top10_channels)
		selected_topics = try values.decodeIfPresent([String].self, forKey: .selected_topics)
		selected_channels = try values.decodeIfPresent([String].self, forKey: .selected_channels)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
