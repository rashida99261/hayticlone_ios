
import Foundation
struct privacyModel : Codable {
    let Title : String?
    let Description : String?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case Title = "Title"
        case Description = "Description"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Title = try values.decodeIfPresent(String.self, forKey: .Title)
        Description = try values.decodeIfPresent(String.self, forKey: .Description)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}


struct commonResponseModel : Codable {
    let status : Int?
    let message : String?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}

// create folder resonse as well
//{
//    "status": "45",
//    "messaage": " Folder created successfully",
//    "method": "GET"
//}

struct locationModel : Codable {
    let status : Int?
    let message : String?
    let method : String?
    let Userid : String?

    enum CodingKeys: String, CodingKey {

        case Userid = "Userid"
        case status = "status"
        case message = "message"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Userid = try values.decodeIfPresent(String.self, forKey: .Userid)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}

struct notificationModel : Codable {
    let Status : Int?
    let message : String?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case Status = "Status"
        case message = "message"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Status = try values.decodeIfPresent(Int.self, forKey: .Status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}

struct viewCountsModel : Codable {
    
    let return_info : Int?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case return_info = "return_info"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        return_info = try values.decodeIfPresent(Int.self, forKey: .return_info)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}

struct addFollowingModel : Codable {
    
   let following_Status : Int?
    let message : String?
    let method : String?

    enum CodingKeys: String, CodingKey {

        case following_Status = "Following Status"
        case message = "message"
        case method = "method"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        following_Status = try values.decodeIfPresent(Int.self, forKey: .following_Status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        method = try values.decodeIfPresent(String.self, forKey: .method)
    }

}


