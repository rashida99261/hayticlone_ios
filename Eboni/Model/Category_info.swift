
import Foundation
struct Category_info : Codable {
	var cat_name : String?
	var cat_id : String?
	var featured : String?

	enum CodingKeys: String, CodingKey {

		case cat_name = "cat_name"
		case cat_id = "cat_id"
		case featured = "featured"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		cat_name = try values.decodeIfPresent(String.self, forKey: .cat_name)
		cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
		featured = try values.decodeIfPresent(String.self, forKey: .featured)
	}
    

}
