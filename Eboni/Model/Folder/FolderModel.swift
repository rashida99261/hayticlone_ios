
import Foundation
struct FolderModel : Codable {
	let folderList : [FolderList]?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case folderList = "folderList"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		folderList = try values.decodeIfPresent([FolderList].self, forKey: .folderList)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
