
import Foundation
struct catModel : Codable {
	var category_info : [Category_info]?
	let method : String?

	enum CodingKeys: String, CodingKey {

		case category_info = "category_info"
		case method = "method"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		category_info = try values.decodeIfPresent([Category_info].self, forKey: .category_info)
		method = try values.decodeIfPresent(String.self, forKey: .method)
	}

}
