
import Foundation
struct BlackHistory_info : Codable {
    
	let summery : String?
	let blackDate : String?

	enum CodingKeys: String, CodingKey {
		case summery = "summery"
		case blackDate = "blackDate"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		summery = try values.decodeIfPresent(String.self, forKey: .summery)
		blackDate = try values.decodeIfPresent(String.self, forKey: .blackDate)
	}
}
