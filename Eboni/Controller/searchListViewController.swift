//
//  searchListViewController.swift
//  Eboni
//
//  Created by Apple on 23/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class searchListViewController: UIViewController {
    
    
    var searchArticleList:[AnyObject] = []
    
    var cat_Id = ""
    
    var totalPageCount: Int?
    
    var viewModel : articleListVM!
    
    var refreshTbl: UIRefreshControl!
    
    @IBOutlet weak var tblArticleList : UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var addsIndex = 5
    
    var scrollIndex = 1
    var fetchingSearchData = false
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var hgtFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                        
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        self.tblArticleList.tableFooterView = UIView()
        self.tblArticleList.isHidden = true
        // Do any additional setup after loading the view.
        
        self.viewFooter.isHidden = true
        self.hgtFooter.constant = 0
        self.spinner.isHidden = true
        
        self.view.bringSubviewToFront(viewFooter)
        
        self.loadBanner(self.bannerView)
        
        refreshTbl = UIRefreshControl()
        refreshTbl.addTarget(self, action: #selector(onRefreshTbl), for: .valueChanged)
        tblArticleList.addSubview(refreshTbl)
        
        
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        scrollIndex = 1
        self.searchArticleList.removeAll()
        self.searchText.text = ""
        self.tblArticleList.isHidden = true
        
    }
    
    func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    
    @objc func onRefreshTbl() {
        
        run(after: 2) {
            self.scrollIndex = 1
            self.searchArticleList = []
            APIClient.showLoaderView(view: self.view)
            self.seearchArticlesApi(strCkeck: "get", strText: self.searchText.text!)
            
            self.refreshTbl.endRefreshing()
        }
    }
}


extension searchListViewController {
    
    func seearchArticlesApi(strCkeck: String, strText: String){
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.searchArticleList = []
        }
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        APIClient.searchArticlebyKeyword(userId: userId, pageNo: "\(self.scrollIndex)", keyword: strText) { (belowModel) in
            OperationQueue.main.addOperation {
                
                APIClient.hideLoaderView(view: self.view)
                //self.searchText.resignFirstResponder()
                
                if let dict = belowModel as? NSDictionary{
                    
                    let total_no_of_page = dict["total_no_of_page"] as? Int
                    
                    if let arrData = dict["allarticlesLists"] as? [NSDictionary]{
                        
                        if(strCkeck == "get"){
                            if(arrData.count == 0){
                                self.tblArticleList.isHidden = true
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                            else{
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.searchArticleList.append(obj)
                                }
                                
                                
                                for i in 0..<self.searchArticleList.count{
                                    if(i == self.addsIndex){
                                        self.searchArticleList.insert("Insert_banner" as AnyObject, at: self.addsIndex)
                                        self.addsIndex += 6
                                    }
                                }
                                
                                self.viewModel = articleListVM(controller: self, items: self.searchArticleList)
                                self.viewModel.searchVc = self
                                self.viewModel.viewContoller = self
                                self.viewModel.typeVC = "search"
                                self.tblArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                self.tblArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                             forCellReuseIdentifier: "bannerAdsCell")
                                self.tblArticleList.delegate = self
                                self.tblArticleList.dataSource = self
                                self.fetchingSearchData = false
                                self.tblArticleList.isHidden = false
                                self.tblArticleList.reloadData()
                                
                                
                                self.totalPageCount = total_no_of_page
                            }
                        }
                        else{
                            
                            if(arrData.count > 0){
                                for i in 0..<arrData.count{
                                    let obj = arrData[i]
                                    self.searchArticleList.append(obj)
                                }
                                
                                
                                for i in 0..<self.searchArticleList.count{
                                    if(i == self.addsIndex){
                                        self.searchArticleList.insert("Insert_banner" as AnyObject, at: self.addsIndex)
                                        self.addsIndex += 6
                                    }
                                }
                                
                                
                                self.totalPageCount = total_no_of_page
                                self.viewModel = articleListVM(controller: self, items: self.searchArticleList)
                                self.viewModel.searchVc = self
                                self.viewModel.viewContoller = self
                                self.viewModel.typeVC = "search"
                                self.tblArticleList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                                self.tblArticleList.register(UINib(nibName: "bannerAdsCell", bundle: nil),
                                                             forCellReuseIdentifier: "bannerAdsCell")
                                self.tblArticleList.delegate = self
                                self.tblArticleList.dataSource = self
                                self.fetchingSearchData = false
                                self.tblArticleList.isHidden = false
                                self.tblArticleList.reloadData()
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                            else{
                                self.tblArticleList.isHidden = false
                                self.viewFooter.isHidden = true
                                self.hgtFooter.constant = 0
                                self.spinner.isHidden = true
                            }
                        }
                    }
                }
            }
        }
    }
}

extension searchListViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = searchText.text
        var finalString = ""
        if string.count > 0 { // if it was not delete character
            finalString = string2! + string1
        }
        else if string2!.count > 0{ // if it was a delete character
            finalString = String(string2!.dropLast())
            if(finalString.count < 3){
                scrollIndex = 1
                self.searchArticleList.removeAll()
                self.tblArticleList.isHidden = true
            }
        }
        if(finalString.count >= 3){
            // APIClient.showLoaderView(view: self.view)
            // let sendStr = String(finalString.filter { !" \n\t\r".contains($0) })
            //   self.searchText.text = sendStr
            
            self.searchArticleList.removeAll()
            self.seearchArticlesApi(strCkeck: "get", strText: finalString)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension searchListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblArticleList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblArticleList)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblArticleList)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblArticleList)
    }
}


extension searchListViewController{
    func passToViewController(strUrl : String, strArticleId : String, strTitle: String, strFav: String, strLikeC: String, strAlias: String){
        let detail = self.storyboard?.instantiateViewController(identifier: "detailViewContoller") as! detailViewContoller
        detail.passUrl = strUrl
        detail.passArticleId = strArticleId
        detail.isFav = strFav
        detail.likeCount = strLikeC
        detail.nodealias = strAlias

        self.navigationController?.pushViewController(detail, animated: true)
    }
    
    func passToPublisherVC(strUrl : String){
        let detail = self.storyboard?.instantiateViewController(identifier: "publisherListViewController") as! publisherListViewController
        detail.publishrId = strUrl
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

extension searchListViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
