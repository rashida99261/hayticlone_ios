//
//  userExistVC.swift
//  Eboni
//
//  Created by Apple on 25/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class userExistVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)


        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
            
    }

    @IBAction func clickONLoginanotherBtn(_ sender: UIButton)
    {
        let home = self.storyboard?.instantiateViewController(identifier: "loginViewController") as! loginViewController
        self.navigationController?.pushViewController(home, animated: true)

            
    }

    

}
