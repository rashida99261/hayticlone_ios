//
//  imgUplodViewController.swift
//  Eboni
//
//  Created by Apple on 27/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class imgUplodViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var imgProfile : UIImageView!
    var imagePicker: ImagePicker!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)
        
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                self.imgProfile.layer.borderWidth = 4
                self.imgProfile.layer.borderColor = UIColor.black.cgColor
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                self.imgProfile.layer.borderWidth = 4
                self.imgProfile.layer.borderColor = UIColor.white.cgColor
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                        self.imgProfile.layer.borderWidth = 4
                        self.imgProfile.layer.borderColor = UIColor.white.cgColor
                        
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                        self.imgProfile.layer.borderWidth = 4
                        self.imgProfile.layer.borderColor = UIColor.black.cgColor
                        
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    self.imgProfile.layer.borderWidth = 4
                    self.imgProfile.layer.borderColor = UIColor.white.cgColor
                    
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    self.imgProfile.layer.borderWidth = 4
                    self.imgProfile.layer.borderColor = UIColor.black.cgColor
                    
                }
            }
        }
        
        let fname = userGlobal?.first_name
        let lname = userGlobal?.last_name
        self.lblName.text = "\(fname!) \(lname!)"
        self.lblEmail.text = userGlobal?.email
        
        self.setUpImagffromApi()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnAddProfileImgBtn(_ sender: UIButton)
    {
        self.imagePicker.present(from: sender)
    }
}

extension imgUplodViewController {
    
    func setUpImagffromApi(){
        
        APIClient.showLoaderView(view: self.view)
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        
        APIClient.getUerDetails(userId: userId) { (response) in
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                if let dict = response as? NSDictionary {
                    if let ThumbnailUrls = dict["ThumbnailUrls"] as? String
                    {
                        if(ThumbnailUrls != ""){
                            self.imgProfile.sd_setImage(with: URL.init(string: ThumbnailUrls), placeholderImage: UIImage(named: "unprofileIcon"))
                        }
                        else{
                            self.imgProfile.image = #imageLiteral(resourceName: "unprofileIcon")
                        }
                    }
                }
            }
        }
    }
}

extension imgUplodViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
            
        }
        else if(sender.tag == 30){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
                
                // self.present(detail, animated: true, completion: nil)
            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}


extension imgUplodViewController : ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.imgProfile.image = image?.fixedOrientation()
        
        if let imgData = self.imgProfile.image?.jpeg(.low) {
            
            var userId = ""
            if(userGlobal?.id == nil){
                userId = "0"
            }
            else{
                userId = (userGlobal?.id)!
            }
            
            APIClient.showLoaderView(view: self.view)
            
            let filename = "\(Date().timeIntervalSince1970).png"
            
            // generate boundary string using a unique per-app string
            let boundary = UUID().uuidString
            
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            // Set the URLRequest to POST and to the specified URL
            let url = "\(Endpoints.Environment.baseURL)-\(Endpoints.Setting.user_tumbnail)?userid=\(userId)"
            var urlRequest = URLRequest(url: URL(string: url)!)
            urlRequest.httpMethod = "POST"
            
            // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
            // And the boundary is also set here
            urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            var data = Data()
            
            // Add the reqtype field and its value to the raw http request data
            
            // Add the image data to the raw http request data
            data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
            data.append("Content-Disposition: form-data; name=\"profile_thumb\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
            data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
            data.append(imgData)
            
            // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
            // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
            data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
            
            // Send a POST request to the URL, with the data we created earlier
            session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
                
                if(error != nil){
                    print("\(error!.localizedDescription)")
                }
                
                guard let responseData = responseData else {
                    print("no response data")
                    return
                }
                
                if let responseString = String(data: responseData, encoding: .utf8) {
                    print("uploaded to: \(responseString)")
                    
                    OperationQueue.main.addOperation {
                        APIClient.hideLoaderView(view: self.view)
                        
                        if let data = responseString.data(using: String.Encoding.utf8) {
                            
                            do {
                                let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                                if let myDictionary = dictonary
                                {
                                    let msg = myDictionary["message"] as! String
                                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: msg)
                                    self.viewDidLoad()
                                }
                            } catch let error as NSError {
                                print(error)
                            }
                        }
                    }
                }
            }).resume()
        }
    }
}
