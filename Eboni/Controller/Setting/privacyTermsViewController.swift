//
//  privacyTermsViewController.swift
//  Eboni
//
//  Created by Apple on 26/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class privacyTermsViewController: UIViewController {
    
    var isOpenFrom = ""
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var txtVDescription: UITextView!
     @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
                
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        APIClient.showLoaderView(view: self.view)
        self.callApi()
    }
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension privacyTermsViewController {
    func callApi(){
        APIClient.getPrivacy(strTyp: isOpenFrom) { (privacyModel) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
               // self.lblHeading.text = privacyModel.Title
                self.txtVDescription.text = privacyModel.Description?.htmlToString
            }
        }
    }
}

extension privacyTermsViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)

        }
        else if(sender.tag == 30){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
