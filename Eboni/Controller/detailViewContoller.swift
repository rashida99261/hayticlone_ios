//
//  detailViewContoller.swift
//  Eboni
//
//  Created by Apple on 17/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase


fileprivate let ruleId1 = "MyRuleID 001"
fileprivate let ruleId2 = "MyRuleID 002"


class detailViewContoller: UIViewController{
    
    @IBOutlet weak var imgLogo : UIImageView!
    @IBOutlet weak var imgFolder: UIImageView!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var imgLikes: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    var passUrl = ""
    var passArticleId = ""
    var passTitle = ""
    var isFav = ""
    var likeCount = ""
    var nodealias = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                self.imgFolder.image = UIImage(named: "addFolderIcon")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                self.imgFolder.image = UIImage(named: "addFolderIconDark")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                        self.imgFolder.image = UIImage(named: "addFolderIconDark")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                        self.imgFolder.image = UIImage(named: "addFolderIcon")
                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    self.imgFolder.image = UIImage(named: "addFolderIconDark")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    self.imgFolder.image = UIImage(named: "addFolderIcon")
                }
            }
        }
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)!
        }
        Analytics.logEvent("detail_tap", parameters: [
            "userid": userId
        ])
        
        if(self.isFav == "0"){
            self.imgLikes.image = UIImage(named: "unlike")
        }else{
            self.imgLikes.image = UIImage(named: "like")
        }
        self.lblLikeCount.text = "\(self.likeCount)"
        
        self.loadBanner(self.bannerView)
        
        self.viewConfigurations()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension detailViewContoller {
    
    @IBAction func clickOnTabs(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
            else{
                let detail = self.storyboard?.instantiateViewController(identifier: "createFolderViewController") as! createFolderViewController
                detail.modalPresentationStyle = .overCurrentContext
                detail.articleId = self.passArticleId
                detail.openFrom = "add"
                self.present(detail, animated: true, completion: nil)
            }
            
        }
        else if(sender.tag == 20){
            

            
            let detail = self.storyboard?.instantiateViewController(identifier: "sharePopupViewController") as! sharePopupViewController
            detail.modalPresentationStyle = .overCurrentContext
            detail.strLink = self.passUrl
            detail.nid = self.passArticleId
            detail.strTitle = self.passTitle
            detail.nodeAlias = self.nodealias
            self.present(detail, animated: true, completion: nil)
        }
        
        else if(sender.tag == 30)
        {
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
            }
            else{
                
                if(self.isFav == "0"){
                    self.isFav = "1"
                    var count = Int(self.likeCount)
                    count! += 1
                    self.lblLikeCount.text = "\(count ?? 0)"
                    self.imgLikes.image = UIImage(named: "like")
                }
                else{
                    self.isFav = "0"
                    var count = Int(self.likeCount)
                    if(count! > 0){
                        count! -= 1
                    }else{
                        count = 0
                    }
                    self.lblLikeCount.text = "\(count ?? 0)"
                    self.imgLikes.image = UIImage(named: "unlike")
                }
                
                var userId = ""
                if(userGlobal?.id == nil){
                    userId = "0"
                }
                else{
                    userId = (userGlobal?.id)!
                }
                
                APIClient.likeonArticle(userId: userId, node_id: self.passArticleId, likes: "1") { (resp) in
                    DispatchQueue.main.async {
                        
                    }
                }
                
            }
            
            
        }
    }
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 20){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 100){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}


extension detailViewContoller {
    
    // MARK: - private functions
    
    func viewConfigurations() {
        
        guard let url = URL(string: self.passUrl) else {
            print("Couldn't load create NSURL from: " + self.passUrl)
            return
        }
        // webView.navigationDelegate = self
        
        webView.loadRequest(URLRequest(url: url))
        // webView.load()
    }
    
    fileprivate func showAlert(_ title: String, message: String) {
        let alertController: UIAlertController = UIAlertController(title: title,
                                                                   message: message,
                                                                   preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
}

//extension detailViewContoller: WKNavigationDelegate {
//    
//    
////    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
////
////        if navigationAction.navigationType == WKNavigationType.linkActivated {
////
////            let url = navigationAction.request.url
////            let shared = UIApplication.shared
////
////            if shared.canOpenURL(url!) {
////                shared.openURL(url!)
////            }
////
////            decisionHandler(WKNavigationActionPolicy.cancel)
////        }
////        else
////        {
////            decisionHandler(.allow)
////        }
////    }
//}


extension UIViewController {
    
    func loadBanner(_ view: GADBannerView){
        view.adUnitID = Endpoints.Environment.bannerUniId
        view.rootViewController = self
        view.load(GADRequest())
    }
}
