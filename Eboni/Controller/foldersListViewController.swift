//
//  foldersListViewController.swift
//  Eboni
//
//  Created by Apple on 24/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

class foldersListViewController: UIViewController {
    
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var collFolderList: UICollectionView!
    @IBOutlet weak var viewEditFolder: UIView!
    
    @IBOutlet weak var updateFolderPopup : updateFolder!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    var viewModel : folderListVM!
    var arrfolderList:[allFolderList] = []
    
    var refreshCollection: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateFolderPopup.folderViewDelegate = self
        
        
        
        refreshCollection = UIRefreshControl()
        refreshCollection.addTarget(self, action: #selector(onRefreshTbl), for: .valueChanged)
        collFolderList.addSubview(refreshCollection)
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")

                    }
                }
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")

                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        if(userGlobal?.id == nil){
            if let _ = userDef.value(forKey: "isBack") as? String{
                let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
            else{
                self.viewEditFolder.isHidden = true
                self.viewCollection.isHidden = true
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)

            }
        }
        else{
            
            Analytics.logEvent("folder_tap", parameters: [
                "userid": userGlobal?.id
            ])
            
            self.viewEditFolder.isHidden = true
            self.viewCollection.isHidden = false
            APIClient.showLoaderView(view: self.view)
            self.callFolderListApi()
            
        }
    }
}

extension foldersListViewController {
    
    func run(after wait: TimeInterval, closure: @escaping () -> Void) {
        let queue = DispatchQueue.main
        queue.asyncAfter(deadline: DispatchTime.now() + wait, execute: closure)
    }
    
    
    @objc func onRefreshTbl() {
        
        run(after: 2) {
            self.viewEditFolder.isHidden = true
            self.viewCollection.isHidden = false
            APIClient.showLoaderView(view: self.view)
            self.callFolderListApi()
            self.refreshCollection.endRefreshing()
        }
    }
}

extension foldersListViewController {
    
    func callFolderListApi(){
        
        self.arrfolderList = []
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        
        APIClient.getAllFolderList(userId: userId, pageNo: "1") { (allFolderModel) in
            
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                let arrData = allFolderModel.folderList!
                for i in 0..<arrData.count{
                    let obj = arrData[i]
                    self.arrfolderList.append(obj)
                }
                
                self.viewModel = folderListVM(controller: self, items: self.arrfolderList)
                self.viewModel.vc = self
                self.collFolderList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellWithReuseIdentifier: self.viewModel.identifierItem)
                self.collFolderList.delegate = self
                self.collFolderList.dataSource = self
                
                if self.viewModel.items.count == 0{
                    self.viewCollection.isHidden = true
                    
                }else{
                    self.viewCollection.isHidden = false
                    self.collFolderList.reloadData()
                }
            }
        }
        
        
    }
}

extension foldersListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        viewModel.getItemForRowAt(indexPath, collectionView: collFolderList)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.getSelectRowAt(indexPath, collectionView: collFolderList)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        viewModel.getSizeForItem(collFolderList, collectionViewLayout: collectionViewLayout, indexPath: indexPath)
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        let inset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    //        edgeInsetPadding = Int(inset.left+inset.right)
    //        return inset
    //    }
    //
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return CGFloat(minimumSpacing)
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return CGFloat(minimumSpacing)
    //    }
    
    
}

extension foldersListViewController {
    
    @IBAction func clickOnBottomBtn(_ sender: UIButton)
    {
        if(sender.tag == 20){
            userDef.removeObject(forKey: "isBack")
            let folder = self.storyboard?.instantiateViewController(identifier: "followingViewController") as! followingViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 10){
            userDef.removeObject(forKey: "isBack")
            if(userGlobal?.id == nil){
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
            }
            else{
                let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}

extension foldersListViewController : folderDelegate {
    
    func clickOnBack() {
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        self.viewEditFolder.layer.add(transition, forKey: kCATransition)
        self.viewEditFolder.isHidden = true
        
        
        
    }
    
}

extension foldersListViewController {
    
    func openFolderPopUp(tag: Int){
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        self.viewEditFolder.layer.add(transition, forKey: kCATransition)
        self.viewEditFolder.isHidden = false
        
        
        let obj = self.viewModel.items[tag]
        
        self.updateFolderPopup.txtFolderName.text = obj.folder_name
        
        self.updateFolderPopup.btnSave.tag = tag
        self.updateFolderPopup.btnSave.addTarget(self, action: #selector(saveBtnApi(_:)), for: .touchUpInside)
        
        self.updateFolderPopup.btnDel.tag = tag
        self.updateFolderPopup.btnDel.addTarget(self, action: #selector(DeleteBtnApi(_:)), for: .touchUpInside)
        
    }
    
    @objc func saveBtnApi(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        let obj = self.viewModel.items[sender.tag]
        
        let userId = obj.userid
        let folderId = obj.id!
        
        APIClient.changeFolderName(userId: userId!, folderId: folderId, folderName: self.updateFolderPopup.txtFolderName.text!) { (response) in
            
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = response.status
                if(statsu == 1){
                    self.viewWillAppear(true)
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: response.message!)
                }
            }
        }
    }
    
    @objc func DeleteBtnApi(_ sender: UIButton)
    {
        APIClient.showLoaderView(view: self.view)
        let obj = self.viewModel.items[sender.tag]
        
        let userId = obj.userid
        let folderId = obj.id!
        
        APIClient.deleteFolder(userId: userId!, folderId: folderId) { (response) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
                let statsu = response.status
                if(statsu == 1){
                    self.viewWillAppear(true)
                }
                else{
                    APIClient.showAlertMessage(vc: self, titleStr: "Hayti", messageStr: response.message!)
                }
            }
        }
    }
}
