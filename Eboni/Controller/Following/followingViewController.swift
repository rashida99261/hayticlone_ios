//
//  followingViewController.swift
//  Eboni
//
//  Created by Apple on 20/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

class followingViewController: UIViewController {
    
    @IBOutlet weak var viewList: UIView!
    
    var arrTopTopics:[Top10_topics] = []
    var arrfTopChannels:[Top10_channels] = []
    
    @IBOutlet weak var tblFollowingList : UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    var viewModel : topTopicsListVM!
    var viewModelChannel : topChannelsListVM!
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
                
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        if(userGlobal?.id == nil){
            
            if let _ = userDef.value(forKey: "isBack") as? String{
                
                let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                self.navigationController?.pushViewController(folder, animated: false)
            }
            else{
                
                self.viewList.isHidden = true
                let detail = self.storyboard?.instantiateViewController(identifier: "loginPopupVC") as! loginPopupVC
                detail.modalPresentationStyle = .overCurrentContext
                detail.vc = self
                let navC = UINavigationController(rootViewController: detail)
                navC.navigationBar.isHidden = true
                self.present(navC, animated: true, completion: nil)
            }
        }
        else{
            
            Analytics.logEvent("following_tap", parameters: [
                "userid": userGlobal?.id
            ])
            
            self.viewList.isHidden = false
            self.arrTopTopics.removeAll()
            self.arrfTopChannels.removeAll()
            APIClient.showLoaderView(view: self.view)
            self.callListOfTopTen()
        }
    }
    
}

extension followingViewController {
    
    func callListOfTopTen(){
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        
        APIClient.topTenList(userId: userId) { (FollowingModel) in
            
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                
                let topTopic = FollowingModel.top10_topics!
                for i in 0..<topTopic.count{
                    let obj = topTopic[i]
                    self.arrTopTopics.append(obj)
                }
                
                let topChannel = FollowingModel.top10_channels!
                for i in 0..<topChannel.count{
                    let obj = topChannel[i]
                    self.arrfTopChannels.append(obj)
                }
                
                self.viewModel = topTopicsListVM(controller: self, items: self.arrTopTopics, selectId: FollowingModel.selected_topics!)
                
                self.viewModel.vc = self
                self.viewModel.isType = "top"
                
                self.tblFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                
                self.viewModelChannel = topChannelsListVM(controller: self, items: self.arrfTopChannels, selectId: FollowingModel.selected_channels!)
                self.viewModelChannel.vc = self
                self.viewModelChannel.isTyp = "top"
                
                self.tblFollowingList.register(UINib(nibName: self.viewModelChannel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModelChannel.identifierItem)
                
                self.tblFollowingList.delegate = self
                self.tblFollowingList.dataSource = self
                
                let headerNib = UINib.init(nibName: "CustomfollowingHeaderview", bundle: Bundle.main)
                self.tblFollowingList.register(headerNib, forHeaderFooterViewReuseIdentifier: "CustomfollowingHeaderview")
                
                if self.viewModel.items.count == 0 || self.viewModelChannel.items.count == 0{
                    self.tblFollowingList.isHidden = true
                    
                }else{
                    self.tblFollowingList.isHidden = false
                    self.tblFollowingList.reloadData()
                }
            }
            
        }
    }
}

extension followingViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0){
            return viewModel.getNumbersOfRows(in: section)
        }
        else if(section == 1){
            return viewModelChannel.getNumbersOfRows(in: section)
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if(indexPath.section == 0){
            return viewModel.getCellForRowAt(indexPath, tableView: tblFollowingList)
        }
        else if(indexPath.section == 1){
            return viewModelChannel.getCellForRowAt(indexPath, tableView: tblFollowingList)
        }
        else{
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0){
            return viewModel.getHeightForRowAt(indexPath, tableView: tblFollowingList)
        }
        else if(indexPath.section == 1){
            return viewModelChannel.getHeightForRowAt(indexPath, tableView: tblFollowingList)
        }
        else{
            return 0.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            return viewModel.willDisplayCell(indexPath, tableView: tblFollowingList)
        }
        else if(indexPath.section == 1){
            return viewModelChannel.willDisplayCell(indexPath, tableView: tblFollowingList)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            return viewModel.didSelectRowAt(indexPath, tableView: tblFollowingList)
        }
        else if(indexPath.section == 1){
            return viewModelChannel.didSelectRowAt(indexPath, tableView: tblFollowingList)
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let view = tableView.dequeueReusableHeaderFooterView(
                withIdentifier: CustomfollowingHeaderview.reuseIdentifier)
                as? CustomfollowingHeaderview
        else {
            return nil
        }
        
        if(section == 0){
            view.lblTitle.text = "CATEGORIES"
            view.lblsubtitle.text = "Check or uncheck categories to only view content from your favorite categories."
        }
        else if(section == 1){
            view.lblTitle.text = "PUBLISHERS"
            view.lblsubtitle.text = "Check or uncheck publishers to only view content from your favorite publishers."
            
        }
        
        view.btnShowAll.tag = section
        view.btnShowAll.addTarget(self, action: #selector(clickOnShowAllBtn(_:)), for: .touchUpInside)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(section == 0){
            return 100
        }
        else if(section == 1){
            return 125
        }
        return 0
    }
    
    @objc func clickOnShowAllBtn(_ sender: UIButton)
    {
        if(sender.tag == 0){
            let detail = self.storyboard?.instantiateViewController(identifier: "allChannelListViewController") as! allChannelListViewController
            self.navigationController?.pushViewController(detail, animated: true)
        }
        else if(sender.tag == 1){
            let detail = self.storyboard?.instantiateViewController(identifier: "allPublisherListViewController") as! allPublisherListViewController
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
}

extension followingViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = searchText.text
        var finalString = ""
        
        if string.isEmpty
        {
            finalString = String(string2!.dropLast())
        }
        else
        {
            finalString = string2! + string1
        }
        let item = self.viewModel.items.filter({($0.cat_name!.localizedCaseInsensitiveContains(finalString))})
        if item.count > 0
        {
            viewModel.Filtereditems = []
            viewModel.Filtereditems = item
        }
        else
        {
            viewModel.Filtereditems = []
            if(finalString == ""){
                viewModel.Filtereditems = viewModel.items
            }
        }
        
        let itemSec = self.viewModelChannel.items.filter({($0.wEB_NAME!.localizedCaseInsensitiveContains(finalString))})
        if itemSec.count > 0
        {
            viewModelChannel.Filtereditems = []
            viewModelChannel.Filtereditems = itemSec
        }
        else
        {
            viewModelChannel.Filtereditems = []
            if(finalString == ""){
                viewModelChannel.Filtereditems = viewModelChannel.items
            }
        }
        self.tblFollowingList.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension followingViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
