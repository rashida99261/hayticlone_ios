//
//  allPublisherListViewController.swift
//  Eboni
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class allPublisherListViewController: UIViewController {
    
    var arrCategory:[Top10_channels] = []
    
    @IBOutlet weak var tblChnlFollowingList : UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    var viewModel : topChannelsListVM!
    
    var selectChannel = [String]()
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var hgtFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!
    
    var scrollIndex = 1
    var fetchingData = false
    
    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewFooter.isHidden = true
        self.hgtFooter.constant = 0
        self.spinner.isHidden = true
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
                
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        APIClient.showLoaderView(view: self.view)
        self.callSelectedFollowing()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension allPublisherListViewController {
    
    func callSelectedFollowing(){
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        APIClient.selectedFollowing(userId: userId) { (selectModel) in
            OperationQueue.main.addOperation {
                self.selectChannel = selectModel.channels!
                self.callListOfTopTen(strCkeck: "get")
            }
        }
    }
    
    func callListOfTopTen(strCkeck: String){
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.arrCategory = []
        }
        
        APIClient.channelList(page: "\(scrollIndex)") { (allChannel) in
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                let topTopic = allChannel.allwebnames!
                self.parseResponseToTable(strCkeck: strCkeck, topTopic: topTopic)
            }
        }
    }
    
    func parseResponseToTable(strCkeck: String, topTopic: [Top10_channels]){
        
        if(strCkeck == "get"){
            if(topTopic.count == 0){
                
                self.tblChnlFollowingList.isHidden = true
                self.viewFooter.isHidden = true
                self.hgtFooter.constant = 0
                self.spinner.isHidden = true
            }
            else{
                
                for i in 0..<topTopic.count{
                    let obj = topTopic[i]
                    self.arrCategory.append(obj)
                }
                
                self.viewModel = topChannelsListVM(controller: self, items: self.arrCategory, selectId: self.selectChannel)
                
                self.viewModel.viewCntrl = self
                self.viewModel.isTyp = "all"
                
                self.tblChnlFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                
                self.tblChnlFollowingList.delegate = self
                self.tblChnlFollowingList.dataSource = self
                
                self.fetchingData = false
                self.tblChnlFollowingList.isHidden = false
                self.tblChnlFollowingList.reloadData()
                
            }
            
        }
        else{
            
            if(topTopic.count > 0){
                
                for i in 0..<topTopic.count{
                    let obj = topTopic[i]
                    self.arrCategory.append(obj)
                }
                
                self.viewModel = topChannelsListVM(controller: self, items: self.arrCategory, selectId: self.selectChannel)
                
                
                self.viewModel.viewCntrl = self
                self.viewModel.isTyp = "all"
                
                self.tblChnlFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                
                self.tblChnlFollowingList.delegate = self
                self.tblChnlFollowingList.dataSource = self
                
                self.fetchingData = false
                
                self.tblChnlFollowingList.isHidden = false
                self.tblChnlFollowingList.reloadData()
                
                self.viewFooter.isHidden = true
                self.hgtFooter.constant = 0
                self.spinner.isHidden = true
                
            }
            else{
                self.viewFooter.isHidden = true
                self.hgtFooter.constant = 0
                self.spinner.isHidden = true
            }
        }
        
    }
    
    
    func searchListOfTopic(strCkeck: String, strKey: String){
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        
        self.arrCategory = []
        
        APIClient.SearchchannelList(page: "\(scrollIndex)", keyword: strKey, userId: userId) { (allChannel) in
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                let topTopic = allChannel.allwebnames!
                for i in 0..<topTopic.count{
                    let obj = topTopic[i]
                    self.arrCategory.append(obj)
                }
                
                self.viewModel = topChannelsListVM(controller: self, items: self.arrCategory, selectId: self.selectChannel)
                
                self.viewModel.viewCntrl = self
                self.viewModel.isTyp = "search"
                
                self.tblChnlFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                
                self.tblChnlFollowingList.delegate = self
                self.tblChnlFollowingList.dataSource = self
                
                self.fetchingData = false
                self.tblChnlFollowingList.isHidden = false
                self.tblChnlFollowingList.reloadData()
                
            }
        }
    }
    
}

extension allPublisherListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCellForRowAt(indexPath, tableView: tblChnlFollowingList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblChnlFollowingList)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblChnlFollowingList)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblChnlFollowingList)
    }
    
    
}

extension allPublisherListViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = searchText.text
        var finalString = ""
        
        if string.isEmpty
        {
            finalString = String(string2!.dropLast())
            if(finalString == ""){
                self.searchText.text = ""
                APIClient.showLoaderView(view: self.view)
                self.callListOfTopTen(strCkeck: "get")
            }
        }
        else if string.count > 0
        {
            finalString = string2! + string1
        }
            
        else if string2!.count > 0{ // if it was a delete character
            finalString = String(string2!.dropLast())
            if(finalString.count < 2){
                scrollIndex = 1
                self.arrCategory = []
                self.tblChnlFollowingList.isHidden = true
            }
        }
        if(finalString.count >= 2){
            //APIClient.showLoaderView(view: self.view)
           // self.searchText.text = finalString
            self.arrCategory = []
            self.searchListOfTopic(strCkeck: "get", strKey: finalString)
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension allPublisherListViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}

