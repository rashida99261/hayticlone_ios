//
//  allChannelListViewController.swift
//  Eboni
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class allChannelListViewController: UIViewController {

    
    var arrCategory:[Top10_topics] = []
    
    @IBOutlet weak var tblChnlFollowingList : UITableView!
    @IBOutlet weak var searchText: UITextField!
    
    var viewModel : topTopicsListVM!
    
    var selectTopic = [String]()
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var hgtFooter: NSLayoutConstraint!
    @IBOutlet weak var spinner : UIActivityIndicatorView!

    var scrollIndex = 1
    var fetchingData = false

    @IBOutlet weak var imgLogo : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.viewFooter.isHidden = true
        self.hgtFooter.constant = 0
        self.spinner.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)


    }
    
    override func viewWillAppear(_ animated: Bool){
        
        if let mode = userDef.value(forKey: "isMode") as? String
        {
            if(mode == "light"){
                self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
            }
            else if(mode == "dark"){
                self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
            }
            else if(mode == "auto"){
                
                if #available(iOS 13.0, *) {
                    if UITraitCollection.current.userInterfaceStyle == .dark {
                        self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                    }
                    else {
                        self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                    }
                }
                
            }
        }
        else{
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    self.imgLogo.image = #imageLiteral(resourceName: "whiteLogo")
                }
                else {
                    self.imgLogo.image = #imageLiteral(resourceName: "Logo_TRANSPARENT_COLOR")
                }
            }
        }
        
        APIClient.showLoaderView(view: self.view)
        self.callSelectedFollowing()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension allChannelListViewController {
    
    func callSelectedFollowing(){
        
        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        APIClient.selectedFollowing(userId: userId) { (selectModel) in
            OperationQueue.main.addOperation {
                self.selectTopic = selectModel.topics!
                self.callListOfTopTen(strCkeck: "get")
            }
        }
    }
    
    func callListOfTopTen(strCkeck: String){
        
        if(strCkeck == "get"){
            scrollIndex = 1
            self.arrCategory = []
        }

        var userId = ""
        if(userGlobal?.id == nil){
            userId = "0"
        }
        else{
            userId = (userGlobal?.id)! //32
        }
        
        APIClient.catfollowingList(userId: userId, page: "\(scrollIndex)") { (allFollowingModel) in
            
            OperationQueue.main.addOperation {
                APIClient.hideLoaderView(view: self.view)
                
                let topTopic = allFollowingModel.category_info!
                
                if(strCkeck == "get"){
                    if(topTopic.count == 0){
                        
                        self.tblChnlFollowingList.isHidden = true
                        self.viewFooter.isHidden = true
                        self.hgtFooter.constant = 0
                        self.spinner.isHidden = true
                    }
                    else{
                        
                        for i in 0..<topTopic.count{
                            let obj = topTopic[i]
                            self.arrCategory.append(obj)
                        }
                        
                        self.viewModel = topTopicsListVM(controller: self, items: self.arrCategory, selectId: self.selectTopic)
                        self.viewModel.viewCntollr = self
                        self.viewModel.isType = "all"
                        
                        self.tblChnlFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                        
                        self.tblChnlFollowingList.delegate = self
                        self.tblChnlFollowingList.dataSource = self
                        
                        self.fetchingData = false
                        self.tblChnlFollowingList.isHidden = false
                        self.tblChnlFollowingList.reloadData()

                    }

                }
                else{
                    
                    if(topTopic.count > 0){
                        
                        for i in 0..<topTopic.count{
                            let obj = topTopic[i]
                            self.arrCategory.append(obj)
                        }
                        
                        self.viewModel = topTopicsListVM(controller: self, items: self.arrCategory, selectId: self.selectTopic)
                        self.viewModel.viewCntollr = self
                        self.viewModel.isType = "all"
                        
                        self.tblChnlFollowingList.register(UINib(nibName: self.viewModel.nibItem, bundle: nil), forCellReuseIdentifier: self.viewModel.identifierItem)
                        
                        self.tblChnlFollowingList.delegate = self
                        self.tblChnlFollowingList.dataSource = self
                        
                        self.fetchingData = false
                        
                        self.tblChnlFollowingList.isHidden = false
                        self.tblChnlFollowingList.reloadData()

                        self.viewFooter.isHidden = true
                        self.hgtFooter.constant = 0
                        self.spinner.isHidden = true

                    }
                    else{
                        self.viewFooter.isHidden = true
                        self.hgtFooter.constant = 0
                        self.spinner.isHidden = true
                    }
                }
                
            }
        }
    }
}

extension allChannelListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumbersOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return viewModel.getCellForRowAt(indexPath, tableView: tblChnlFollowingList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRowAt(indexPath, tableView: tblChnlFollowingList)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        return viewModel.willDisplayCell(indexPath, tableView: tblChnlFollowingList)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return viewModel.didSelectRowAt(indexPath, tableView: tblChnlFollowingList)
        
    }
    
    
}

extension allChannelListViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string1 = string
        let string2 = searchText.text
        var finalString = ""
        
        if string.isEmpty
        {
            finalString = String(string2!.dropLast())
        }
        else
        {
            finalString = string2! + string1
        }
        let item = self.viewModel.items.filter({($0.cat_name!.localizedCaseInsensitiveContains(finalString))})
        if item.count > 0
        {
            viewModel.Filtereditems = []
            viewModel.Filtereditems = item
        }
        else
        {
            viewModel.Filtereditems = []
            if(finalString == ""){
                viewModel.Filtereditems = viewModel.items
            }
        }
        
        self.tblChnlFollowingList.reloadData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

extension allChannelListViewController {
    
    @IBAction func clickOnBottomView(_ sender: UIButton)
    {
        if(sender.tag == 10){
            let folder = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 30){
            let folder = self.storyboard?.instantiateViewController(identifier: "foldersListViewController") as! foldersListViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
        else if(sender.tag == 40){
            let folder = self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! settingsViewController
            self.navigationController?.pushViewController(folder, animated: false)
        }
    }
}
