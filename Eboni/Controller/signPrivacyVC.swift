//
//  signPrivacyVC.swift
//  Eboni
//
//  Created by Apple on 27/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class signPrivacyVC: UIViewController {

    var isOpenFrom = ""
    
    @IBOutlet weak var txtVDescription: UITextView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(addforNotification(_:)), name: .addNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .articleForNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(matchforNotification(_:)), name: .deepLinking, object: nil)

        
        APIClient.showLoaderView(view: self.view)
        self.callApi()
    }
    
    @IBAction func clickONBAckBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension signPrivacyVC {
    func callApi(){
        APIClient.getPrivacy(strTyp: isOpenFrom) { (privacyModel) in
            DispatchQueue.main.async {
                APIClient.hideLoaderView(view: self.view)
               // self.lblHeading.text = privacyModel.Title
                self.txtVDescription.text = privacyModel.Description?.htmlToString
            }
        }
    }
}

